<?php
    require_once('../Product.php');

    if(isset($_POST['ProductType']))
    {
        $newProduct = $_POST['ProductType'];
        $product = new $newProduct();
        $product->setSharedProperties($_POST['SKU'],$_POST['Name'],$_POST['Price']);
        $product->setProperties($propertyArray);
        $product->connectToDatabase();
        $product->setProduct();
        $product->setProductType();
        $product->redirectToHomepage();
        $product->disconnectFromDatabase();
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="product-add.css">
        <title>Add new product</title>
    </head>
    <body class="d-flex flex-column min-vh-100">
        <div class="wrapper flex-grow-1">
            <div class="flex-container d-flex justify-content-center">
                <div class="col-10">
                    <nav class="navbar navbar-expand-lg border-bottom">
                        <a class="navbar-brand">Product List</a>
                        <div class="ml-auto">
                            <button type="submit" form="product_form" class="btn btn-primary text-light mx-2">Save</button>
                            <a type="button" class="btn btn-danger text-light mx-2" href="../">Cancel</a>
                        </div>
                    </nav>
                </div>
            </div>

            <div class="flex-container d-flex justify-content-center">
                <div class="col-10 my-3">

                    <form method="POST" id="product_form" action="product-add.php">
                        <div class="row my-3 mx-1">
                            <label for="sku" class="col-form-label" style="width: 5rem;">SKU</label>
                            <input type="text" id="sku" class="form-control d-inline" name="SKU" placeholder="SKU000000" required="true" style="width: 12rem;">
                        </div>

                        <div class="row my-3 mx-1">
                            <label for="name" class="col-form-label" style="width: 5rem;">Name</label>
                            <input type="text" id="name" class="form-control d-inline" name="Name" placeholder="Name" required="true" style="width: 12rem;">
                        </div>

                        <div class="row my-3 mx-1">
                            <label for="price" class="col-form-label" style="width: 5rem;">Price ($)</label>
                            <input type="text" id="price" class="form-control d-inline" name="Price" placeholder="0.00" required="true"  maxlength="6" style="width: 12rem;">
                        </div>

                        <div class="my-3 mx-1">
                            <select id="productType" class="custom-select text-center" aria-label="ProductType" name="ProductType" style="width: 10rem;" required>
                                <option disabled selected value>Type Switcher</option>
                                <option class="dropdown-item" id="DVD" value="DVD">DVD</option>
                                <option class="dropdown-item" id="Furniture" value="Furniture">Furniture</option>
                                <option class="dropdown-item" id="Book" value="Book">Book</option>
                            </select>
                        </div>
                        
                        <div class="row my-3 mx-1">
                            <div id="specialAttribute">

                                <div id="DVDProperties">
                                    <label for="size" class="col-form-label" style="width: 6rem;">Size (MB) </label>
                                    <input type="text" id="size" class="form-control d-inline" name="Size" placeholder="0" required="true"  maxlength="4" style="width: 12rem;">
                                </div>

                                <div id="FurnitureProperties">
                                    <div class="row my-3 mx-1">
                                        <label for="height" class="col-form-label" style="width: 6rem;">Height (cm) </label>
                                        <input type="text" id="height" class="form-control d-inline" name="Height" placeholder="0" required="true"  maxlength="4" style="width: 12rem;">
                                    </div>

                                    <div class="row my-3 mx-1">
                                        <label for="width" class="col-form-label" style="width: 6rem;">Width (cm) </label>
                                        <input type="text" id="width" class="form-control d-inline" name="Width" placeholder="0" required="true"  maxlength="4" style="width: 12rem;">
                                    </div>

                                    <div class="row my-3 mx-1">
                                        <label for="length" class="col-form-label" style="width: 6rem;">Length (cm) </label>
                                        <input type="text" id="length" class="form-control d-inline" name="Length" placeholder="0" required="true"  maxlength="4" style="width: 12rem;">
                                    </div>
                                </div>

                                <div id="BookProperties">
                                    <label for="weight" class="col-form-label" style="width: 6rem;">Weight (kg) </label>
                                    <input type="text" id="weight" class="form-control d-inline" name="Weight" placeholder="0" required="true" style="width: 12rem;">
                                </div>
                            </div>
                        </div>

                        <div class="font-weight-bold" id="product_description"></div>
                    </form>

                </div>
            </div>

        </div>

        <div class="flex-container d-flex justify-content-center">
            <footer class="text-center border-top col-10">
                <p class="my-4">Scandiweb Test assignment</p>
            </footer>
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="product-add.js"></script>
    </body>
</html>