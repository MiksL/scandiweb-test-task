<?php
require_once('Product.php');
$dbConnect = new mysqli("localhost", "root", "", "products");

if(isset($_POST['checkbox']))
{
    $sql = "DELETE FROM products WHERE product_id = ?";
    $result = $dbConnect->prepare($sql);
    foreach($_POST['checkbox'] as $checkbox)
    {
        $result->bind_param("i", $checkbox);
        $result->execute();
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="index.css">
        <title>Product list</title>
    </head>
    <body class="d-flex flex-column min-vh-100">
        <div class="wrapper flex-grow-1">
            <div class="flex-container d-flex justify-content-center">
                <div class="col-10">
                    <nav class="navbar navbar-expand-lg border-bottom">
                        <a class="navbar-brand">Product List</a>
                        <div class="ml-auto">
                            <form action="" method="post" id="deleteForm">
                                <a type="button" class="btn btn-dark mx-2" href="product-add">ADD</a>
                                <button type="submit" name="DeleteProducts" class="btn btn-danger text-light mx-2" id="delete-product-btn">MASS DELETE</button>
                            </form> 
                        </div>
                    </nav>
                </div>
            </div>

            <div class="flex-container d-flex justify-content-center">
                <div class="row col-10">
                    <?php
                        $sql = "SELECT *
                        FROM products 
                        LEFT JOIN dvds ON dvd_id = product_id
                        LEFT JOIN furniture ON furniture_id = product_id
                        LEFT JOIN books ON book_id = product_id";
                        $result = $dbConnect->query($sql);

                        if(isset($result))
                        {
                            foreach($result as $row)
                            {
                                $product = new $row['product_type']();
                                $product->setSharedProperties($row['sku'],$row['name'],$row['price']);
                                $product->setProperties($row);
                                $product->getProduct();
                            }
                        }
                    ?>
                </div>
            </div>

        </div>
        <div class="flex-container d-flex justify-content-center">
            <footer class="text-center border-top col-10">
                <p class="my-4">Scandiweb Test assignment</p>
            </footer>
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>